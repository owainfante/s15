"use strict";


/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

const firstName = "Kyle Joshua";
console.log ("First Name: " + firstName);
const lastName = "Infante";
console.log("Last name: " + lastName);
const agePartOne = 25;
console.log("Age: " + agePartOne);
const hobbies = ["Playing Online Games", "Watching Anime", "Playing Tabletop Games"];
console.log("Hobbies: ");
console.log(hobbies);
const workAdress = {houseNumber: "B9 L10", street: "Riverside Street", city: "Naga City", state: "Camarines Sur"};
console.log("Work Address: ");
console.log(workAdress);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
console.log("--------------------------------DEBUG SECTION----------------------------------------------------")

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullNameFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + fullNameFriend);

	let lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);


